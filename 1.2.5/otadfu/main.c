/**
* main.c
*
*
*
*/

/**
* This an example application that demonstrates DFU using BGLIB C function
* definitions.
*
* Most of the functionality in BGAPI uses a request-response-event pattern
* where the module responds to a command with a command response indicating
* it has processed the request and then later sending an event indicating
* the requested operation has been completed.
*
*/

#include <Windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include "gecko_bglib.h"
#include "uart.h"
#include "bg_dfu.h"


BGLIB_DEFINE();

/**
* Configurable parameters that can be modified to match the test setup.
*/

/** The default serial port to use for BGAPI communication. */
uint8_t* default_uart_port = "COM25";
/** The default baud rate to use. */
uint32_t default_baud_rate = 115200;


/** The serial port to use for BGAPI communication. */
static uint8_t* uart_port = NULL;
/** The baud rate to use. */
static uint32_t baud_rate = 0;
/** The external flash slot to use. */
static uint8_t flash_slot = 0;
/** If the bl must boot on the image stored */
static uint8_t boot_file = 0;

/** Usage string */
// Slot in external flash is : 00 : Reset factory image - 01 BLE image - 02 ZigBee image - 03 IBR file
#define USAGE "Usage: %s [serial port] [baud rate] [ota file] [slot] [remote address] \n\n"
/**/
/** dfu file to upload*/
FILE *dfu_file = NULL;
/** remote device address*/
bd_addr remote_address;
/*Bluetooth connection*/
uint8_t ble_connection;
/*found OTA descriptors*/
uint32_t ota_gatt_service_handle;
uint16_t ota_control_characteristic;
uint16_t ota_data_characteristic;

/*OTA UUIDS*/
// Service: 1d14d6ee-fd63-4fa1-bfa4-8f47b42119f0
uint8_t uuid_ota_service[] = { 0xf0, 0x19, 0x21, 0xb4, 0x47, 0x8f, 0xa4, 0xbf, 0xa1, 0x4f, 0x63, 0xfd, 0xee, 0xd6, 0x14, 0x1d };
//Control: f7bf3564-fb6d-4e53-88a4-5e37e0326063
uint8_t uuid_ota_control[] = { 0x63, 0x60, 0x32, 0xe0, 0x37, 0x5e, 0xa4, 0x88, 0x53, 0x4e, 0x6d, 0xfb, 0x64, 0x35, 0xbf, 0xf7 };
//Data: 984227f3-34fc-4045-a5d0-2c581f81a153
uint8_t uuid_ota_data[] = { 0x53, 0xa1, 0x81, 0x1f, 0x58, 0x2c, 0xd0, 0xa5, 0x45, 0x40, 0xfc, 0x34, 0xf3, 0x27, 0x42, 0x98 };

/*Error macro*/
#define ERROR_EXIT(...) do{printf(__VA_ARGS__);exit(EXIT_FAILURE);}while(0)

/* OTA Version */
#define OTA_VERSION 0x11	//1.1

/*
Enumeration of possible OTA states
*/
enum ota_states
{
	OTA_INIT, // default state
	OTA_CONNECT,// connect to remote device
	OTA_FIND_SERVICES,// find OTA control&data attributes    
	OTA_FIND_CHARACTERISTICS,// find OTA control&data attributes
	OTA_BEGIN,// Begin OTA process
	OTA_READY,// Wait for Server to be ready before to push data
	OTA_UPLOAD,// Upload data to remote device
	OTA_END,// End current OTA process
	OTA_RESTART,// End current OTA process
}ota_state = OTA_INIT;
void ota_change_state(enum ota_states new_state);

///DFU
#define MAX_DFU_PACKET 20
char dfu_data[MAX_DFU_PACKET];
bool dfu_resync = false;
size_t dfu_toload = 0;
size_t dfu_total = 0;
size_t dfu_current_pos = 0;
time_t dfu_start_time;
clock_t dfu_end_time;
clock_t dfu_end_time_diff = 0;
clock_t tempodiff = 0;
clock_t tempostart = 0;
clock_t tempostate = 0;

/*
*/
int dfu_read_size()
{
	if (fseek(dfu_file, 0L, SEEK_END))
		return(-1);
	dfu_total = dfu_toload = ftell(dfu_file);
	if (fseek(dfu_file, 0L, SEEK_SET))
		return(-1);
	printf("Bytes to send:%d\n", dfu_toload);
	fflush(stdout);
	return(0);
}

/**
* Display the reason of OTA fail
*/
void Error_OTA( uint8 Error, uint8 State )
{
	printf("Error OTA Server:");

	// Error from OTA server, check OTAU_BLE_C4Bond_Specification.docx OTAU_Status
	switch (Error)
	{
	case 0x00:
		// Bootloader init failed or OTAU_Init command is wrong
		printf("Init fail\n");
		break;
	case 0x01:
		// Error during external flash write
		printf("External flash write fail\n");
		break;
	case 0x02:
		// At least one frame missied during OTA transfer
		printf("Missing frame\n");
		break;
	case 0x03:
		// The image is not valid
		printf("Verify fail\n");
		break;
	case 0x04:
		// The file sent is too big
		printf("Incorrect file size\n");
		break;
	case 0x05:
		// Timeout server
		printf("Timeout server state - ");
		switch (State)
		{
		case 0x00:
			printf("INIT\n");
			break;
		case 0x01:
			printf("READY\n");
			break;
		case 0x02:
			printf("UPLOAD END\n");
			break;
		case 0x03:
			printf("UPLOAD\n");
			break;
		default:
			printf("Unknow state\n");
			break;
		}
		break;		
	default:
		printf("Unknow error\n");
		break;
	}
}

/*
*/
void send_dfu_block()
{
	time_t ti;
	size_t dfu_size;
	struct gecko_cmd_packet *p;
	size_t writecpt = 0;
	uint32_t rem = 0;
	uint32_t nbFrame = 0;
	dfu_end_time_diff = 0;

	rem = dfu_total % 20; // Division rest
	nbFrame = floor(dfu_total / 20); // Int nb frame

	if (rem != 0)
	{
		nbFrame++;
	}

	while (dfu_toload > 0)
	{
		struct gecko_msg_gatt_write_characteristic_value_without_response_rsp_t  *rsp;

		dfu_size = (dfu_toload > sizeof(dfu_data)) ? sizeof(dfu_data) : dfu_toload;
		if (fread(dfu_data, 1, dfu_size, dfu_file) != dfu_size)
		{
			printf("File read failure\n");
			exit(-1);
		}

		do
		{
			p = gecko_peek_event();
			if (p)
			{
				// Check for events here
				if( BGLIB_MSG_ID(p->header) == gecko_evt_gatt_characteristic_value_id )
				{
					if ((p->data.evt_gatt_characteristic_value.att_opcode == 27) &&
						(p->data.evt_gatt_characteristic_value.characteristic == ota_control_characteristic))
					{
						if ((p->data.evt_gatt_characteristic_value.value.data[0] == 0x03)
						 && (p->data.evt_gatt_characteristic_value.value.data[1] == 0x00))
						{
							Error_OTA(p->data.evt_gatt_characteristic_value.value.data[2], p->data.evt_gatt_characteristic_value.value.data[3]);
							exit(EXIT_SUCCESS);
						}
					}
				}
			}
			rsp = gecko_cmd_gatt_write_characteristic_value_without_response(ble_connection, ota_data_characteristic, dfu_size, dfu_data);
		} while (rsp->result == bg_err_out_of_memory);

		writecpt++;

		if (rsp->result)
			ERROR_EXIT("Error," __FUNCTION__ ", characteristic write failed:%x", rsp->result);

		dfu_current_pos += dfu_size;
		dfu_toload -= dfu_size;

		ti = time(NULL);
		if ((ti != dfu_start_time) &&
			(dfu_total > 0))
		{
			printf("\r%d%% %.2fkbit/s , packet %d/%d         ",
				100 * dfu_current_pos / dfu_total,
				dfu_current_pos * 8.0 / 1000.0 / difftime(ti, dfu_start_time),
				writecpt, nbFrame);
		}
	}
	printf("\n");

	ota_change_state(OTA_END);
}

/*
*/
void sync_boot()
{
	struct gecko_cmd_packet *p;

	// Turn the BLE client device in DFU mode to start the OTA-DFU process from Silbas
	gecko_cmd_dfu_reset(0);

	do
	{
		p = gecko_peek_event();
		if (p)
		{
			switch (BGLIB_MSG_ID(p->header))
			{
			case gecko_evt_system_boot_id:

				struct gecko_msg_sm_set_bondable_mode_rsp_t *bm_rsp;

				printf("Boot OTA client\n");
				fflush(stdout);

				// Security Manager - Device accepts new bondings (1) or not (0)
				bm_rsp = gecko_cmd_sm_set_bondable_mode(1);
				if (bm_rsp->result)
					ERROR_EXIT("Error, bondable mode failed,%x", bm_rsp->result);
				return;
			}
		}
	} while (1);
}

/*
ERROR MANAGEMENT FROM CLIENT SIDE

struct gecko_msg_gatt_write_characteristic_value_rsp_t  *rsp;
// Content of the Control command is as follows: [CMD: 5 for status] [STATUS: 0 for KO, 1 for OK]
dfu_data[0] = 0x05;
dfu_data[1] = 0x00;
rsp = gecko_cmd_gatt_write_characteristic_value( ble_connection, ota_control_characteristic, 2, dfu_data );
if ( rsp->result )
ERROR_EXIT( "Error," __FUNCTION__ ", characteristic write failed,%x", rsp->result );

*/

/*
*/
void ota_change_state(enum ota_states new_state)
{
	ota_state = new_state;
	switch (ota_state)
	{
	case OTA_INIT:
		/* State timeout - 500 ms*/
		tempostate = 500;	

		sync_boot();
		{
			printf("\n<----------------------- OTA INIT ----------------------->\n");
			struct gecko_msg_system_get_bt_address_rsp_t *rsp;
			rsp = gecko_cmd_system_get_bt_address();
			printf("Local address:%02x:%02x:%02x:%02x:%02x:%02x\n",
				rsp->address.addr[5],
				rsp->address.addr[4],
				rsp->address.addr[3],
				rsp->address.addr[2],
				rsp->address.addr[1],
				rsp->address.addr[0]);
		}

		if (dfu_read_size())
			ERROR_EXIT("Error, DFU file read failed\n");
		ota_change_state(OTA_CONNECT);
		break;

	case OTA_CONNECT:
	{
		struct gecko_msg_le_gap_open_rsp_t *rsp;
		struct gecko_msg_le_gap_set_conn_parameters_rsp_t *param_rsp;
		struct gecko_msg_sm_delete_bondings_rsp_t *sm_rsp;

		/* State timeout - 500 ms*/
		tempostate = 500;

		printf("\n<---------------------- OTA CONNECT --------------------->\n");

		// Set default connection parameters
		param_rsp = gecko_cmd_le_gap_set_conn_parameters(10, 10, 0, 300);
		if (param_rsp->result)
			ERROR_EXIT("Error, set connection parameters failed,%x", param_rsp->result);

		// Security Manager - Delete all bonding information
		sm_rsp = gecko_cmd_sm_delete_bondings();
		if (sm_rsp->result)
			ERROR_EXIT("Error, bondable mode failed,%x", sm_rsp->result);

		// Move to connect state, connect to device address
		rsp = gecko_cmd_le_gap_open(remote_address, le_gap_address_type_public);
		if (rsp->result)
			ERROR_EXIT("Error, open failed,%x", rsp->result);
		ble_connection = rsp->connection;
		printf("Connecting...");
		fflush(stdout);
	}
	break;

	case OTA_FIND_SERVICES:
	{
		/* State timeout - 500 ms*/
		tempostate = 500;

		printf("\n<------------------- OTA FIND SERVICES ------------------>\n");
		struct gecko_msg_gatt_discover_primary_services_by_uuid_rsp_t *rsp;
		rsp = gecko_cmd_gatt_discover_primary_services_by_uuid(ble_connection, sizeof(uuid_ota_service), uuid_ota_service);
		if (rsp->result)
			ERROR_EXIT("Error, service discover failed,%x", rsp->result);
		printf("Discovering services...");
		fflush(stdout);
		ota_gatt_service_handle = 0xFFFFFFFF;
	}
	break;

	case OTA_FIND_CHARACTERISTICS:
	{
		/* State timeout - 500 ms*/
		tempostate = 500;

		printf("\n<--------------- OTA FIND CHARACTERISTICS --------------->\n");
		struct gecko_msg_gatt_discover_characteristics_rsp_t  *rsp;
		rsp = gecko_cmd_gatt_discover_characteristics(ble_connection, ota_gatt_service_handle);
		if (rsp->result)
			ERROR_EXIT("Error, characteristics discover failed,%x", rsp->result);
		printf("Discovering characteristics...");
		fflush(stdout);
		ota_control_characteristic = 0xFFFF;
		ota_data_characteristic = 0xFFFF;
	}
	break;

	case OTA_BEGIN:
	{
		/* State timeout - 500ms*/
		tempostate = 500;

		printf("\n<----------------------- OTA BEGIN ---------------------->\n");
		uint8_t size_cmd = 0;
		struct gecko_msg_gatt_write_characteristic_value_rsp_t  *rsp;

		// Content of the Control command is as follows: [CMD: 1 for start] [EBL file size: 4 bytes]  [Flash slot]  [Boot?]
		size_cmd = 1 + 4 + 1 + 1 + 1;
		dfu_data[0] = 0x01;
		dfu_data[1] = (uint8_t)(dfu_total & 0x000000FF);
		dfu_data[2] = (uint8_t)((dfu_total & 0x0000FF00) >> 8);
		dfu_data[3] = (uint8_t)((dfu_total & 0x00FF0000) >> 16);
		dfu_data[4] = (uint8_t)((dfu_total & 0xFF000000) >> 24);
		dfu_data[5] = flash_slot;
		dfu_data[6] = boot_file;
		dfu_data[7] = OTA_VERSION;

		rsp = gecko_cmd_gatt_write_characteristic_value(ble_connection, ota_control_characteristic, size_cmd, dfu_data);
		if (rsp->result)
			ERROR_EXIT("Error," __FUNCTION__ ", characteristic write failed,%x", rsp->result);

		printf("OTA_control DATA: %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n",
			dfu_data[0],
			dfu_data[1],
			dfu_data[2],
			dfu_data[3],
			dfu_data[4],
			dfu_data[5],
			dfu_data[6],
			dfu_data[7]);

		printf("DFU mode...");
		fflush(stdout);
	}
	break;

	case OTA_READY:
		/* State timeout - 8s */
		tempostate = 8000;
		printf("\n<----------------------- OTA READY ---------------------->\n");
		// Write Client Characterisitc Configuration descriptor for notification on OTA_CONTROL attribute
		struct gecko_msg_gatt_set_characteristic_notification_rsp_t  *rsp;
		rsp = gecko_cmd_gatt_set_characteristic_notification(ble_connection, ota_control_characteristic, 1);
		if (rsp->result)
			ERROR_EXIT("Error," __FUNCTION__ ", descriptor write failed,%x", rsp->result);

		printf("Wait for Ready Status from Server...");
		break;

	case OTA_UPLOAD:
		/* State timeout - 500 ms*/
		tempostate = 500;

		printf("\n<---------------------- OTA UPLOAD ---------------------->\n");
		dfu_start_time = time(NULL);
		send_dfu_block();
		break;

	case OTA_END:
	{
		/* State timeout - 2000 ms*/
		tempostate = 5000;

		printf("\n<------------------------ OTA END ----------------------->\n");
		printf("Finishing DFU block...");
		fflush(stdout);
		{
			struct gecko_msg_gatt_write_characteristic_value_rsp_t  *rsp;
			// Content of the Control command is as follows: [CMD: 5 for status] [STATUS: 0 for KO, 1 for OK]
			dfu_data[0] = 0x03;
			dfu_data[1] = 0x01;
			rsp = gecko_cmd_gatt_write_characteristic_value(ble_connection, ota_control_characteristic, 2, dfu_data);
			if (rsp->result)
				ERROR_EXIT("Error," __FUNCTION__ ", characteristic write failed,%x", rsp->result);
			else
				printf("OK\n");
		}
	/*	printf("Closing connection...");
		fflush(stdout);
		{
			struct gecko_msg_endpoint_close_rsp_t  *rsp;
			rsp = gecko_cmd_endpoint_close(ble_connection);
			if (rsp->result)
				ERROR_EXIT("Error," __FUNCTION__ ", close failed,%x", rsp->result);
		}*/
	}
	break;
	}
}

/**
* Function called when a message needs to be written to the serial port.
* @param msg_len Length of the message.
* @param msg_data Message data, including the header.
* @param data_len Optional variable data length.
* @param data Optional variable data.
*/
static void on_message_send(uint16 msg_len, uint8* msg_data)
{
	/** Variable for storing function return values. */
	int ret;

#if DEBUG
	printf("on_message_send()\n");
#endif /* DEBUG */

    ret = uartTx(msg_len, msg_data);
    if(ret < 0)
    {
        printf("on_message_send() - failed to write to serial port %s, ret: %d, errno: %d\n", uart_port, ret, errno);
        exit(EXIT_FAILURE);
    }
}

/*
*/
int parse_address(const char *str, bd_addr *addr)
{
	int a[6];
	int i;
	i = sscanf(str, "%02x:%02x:%02x:%02x:%02x:%02x",
		&a[5],
		&a[4],
		&a[3],
		&a[2],
		&a[1],
		&a[0]
	);
	if (i != 6)
		return(-1);

	for (i = 0; i < 6; i++)
		addr->addr[i] = (uint8_t)a[i];

	return(0);
}

// Usage is: otadfu.exe [UART port] [baud rate] [EBL/DFU filename] [Ext Flash slot] [Boot?] [Remote BDA]
// Example:  otadfu.exe COM5 115200 ota_proj_1_full.dfu 0 0 00:0B:57:00:01:02
// Slot in external flash is: 00: Reset factory image - 01 BLE image - 02 ZigBee image - 03 IBR file
int hw_init(int argc, char* argv[])
{
	if (argc < 7)
	{
		printf(USAGE, argv[0]);
		exit(EXIT_FAILURE);
	}
	// Handle the command-line arguments

	// Filename
	dfu_file = fopen(argv[3], "rb");
	if (dfu_file == NULL)
	{
		printf("cannot open file %s\n", argv[3]);
		exit(-1);
	}
	// Baudrate
	baud_rate = atoi(argv[2]);
	// UART port
	uart_port = argv[1];
	// Remote address
	if (parse_address(argv[6], &remote_address))
	{
		printf("Unable to parse address %s", argv[6]);
		exit(EXIT_FAILURE);
	}
	// Flash slot
	flash_slot = atoi(argv[4]);

	// Boot?
	boot_file = atoi(argv[5]);	

	// Initialise the serial port
    return uartOpen((int8_t*)uart_port, baud_rate, 1, 100);
}

/**
* The main program.
*/
int main(int argc, char* argv[])
{
	struct gecko_cmd_packet *p;
	bool resync = true;
	bool timeout = 0;
	bool close_con = 0;
	/**
	* Initialize BGLIB with our output function for sending messages.
	*/

    BGLIB_INITIALIZE_NONBLOCK(on_message_send, uartRx, uartRxPeek);

	if (hw_init(argc, argv) < 0)
	{
		printf("HW init failure\n");
		exit(EXIT_FAILURE);
	}
	ota_change_state(OTA_INIT);
	while (1)
	{
		tempodiff = 0;
		tempostart = clock();
		timeout = 0; 
		// Wait an event for 500ms
		do
		{
			p = gecko_peek_event();
			tempodiff = clock() - tempostart;
			if (tempodiff >= tempostate)
			{
				timeout = 1;
			}
		} while ((p == NULL) && (!timeout));

		switch (ota_state)
		{
		case OTA_CONNECT:
			switch (BGLIB_MSG_ID(p->header))
			{
			case gecko_evt_le_connection_opened_id:
				printf("OK\n");
				fflush(stdout);

				// Link is established now!
				// Security Manager - Enhance the security of the connection to sart encryption
		/*		struct gecko_msg_sm_increase_security_rsp_t *security_rsp;
				security_rsp = gecko_cmd_sm_increase_security(ble_connection);
				if (security_rsp->result)
					ERROR_EXIT("Error, increase security failed,%x", security_rsp->result);*/

				ota_change_state(OTA_FIND_SERVICES);
				break;

			// Error from Server
			case gecko_evt_gatt_characteristic_value_id:
				
					if ((p->data.evt_gatt_characteristic_value.att_opcode == 27) &&
						(p->data.evt_gatt_characteristic_value.characteristic == ota_control_characteristic))
					{
						if ((p->data.evt_gatt_characteristic_value.value.data[0] == 0x03)
							&& (p->data.evt_gatt_characteristic_value.value.data[1] == 0x00))
						{
							Error_OTA(p->data.evt_gatt_characteristic_value.value.data[2], p->data.evt_gatt_characteristic_value.value.data[3]);
							fflush(stdout);
						}
					}
				ota_change_state(OTA_END);
				break;
				
			default:
				break;
			}
			break;

		case OTA_FIND_SERVICES:
			switch (BGLIB_MSG_ID(p->header))
			{
			case gecko_evt_gatt_procedure_completed_id:
				if (ota_gatt_service_handle == 0xFFFFFFFF)
					ERROR_EXIT("Error, no valid OTA service found");

				printf("OK\n");
				fflush(stdout);
				ota_change_state(OTA_FIND_CHARACTERISTICS);
				break;
			case gecko_evt_gatt_service_id:
				ota_gatt_service_handle = p->data.evt_gatt_service.service;
				break;
				// Error from Server
			case gecko_evt_gatt_characteristic_value_id:

				if ((p->data.evt_gatt_characteristic_value.att_opcode == 27) &&
					(p->data.evt_gatt_characteristic_value.characteristic == ota_control_characteristic))
				{
					if ((p->data.evt_gatt_characteristic_value.value.data[0] == 0x03)
						&& (p->data.evt_gatt_characteristic_value.value.data[1] == 0x00))
					{
						Error_OTA(p->data.evt_gatt_characteristic_value.value.data[2], p->data.evt_gatt_characteristic_value.value.data[3]);
						fflush(stdout);
					}
				}
				ota_change_state(OTA_END);
				break;
			default:
				break;
			}
			break;

		case OTA_FIND_CHARACTERISTICS:
			switch (BGLIB_MSG_ID(p->header))
			{
			case gecko_evt_gatt_procedure_completed_id:
				if ((ota_control_characteristic == 0xFFFF) ||
					(ota_data_characteristic == 0xFFFF))
					ERROR_EXIT("Error, no valid OTA characteristics found");

				printf("OK\n    Control handle:%d\n    Data handle:%d\n", ota_control_characteristic, ota_data_characteristic);
				fflush(stdout);
				ota_change_state(OTA_BEGIN);
				break;

			case gecko_evt_gatt_characteristic_id:
				if ((p->data.evt_gatt_characteristic.uuid.len == sizeof(uuid_ota_control)) &&
					(!memcmp(p->data.evt_gatt_characteristic.uuid.data, uuid_ota_control, sizeof(uuid_ota_control))))
				{
					ota_control_characteristic = p->data.evt_gatt_characteristic.characteristic;
				}
				if ((p->data.evt_gatt_characteristic.uuid.len == sizeof(uuid_ota_data)) &&
					(!memcmp(p->data.evt_gatt_characteristic.uuid.data, uuid_ota_data, sizeof(uuid_ota_data))))
				{
					ota_data_characteristic = p->data.evt_gatt_characteristic.characteristic;
				}
				break;
			// Error from Server
			case gecko_evt_gatt_characteristic_value_id:

				if ((p->data.evt_gatt_characteristic_value.att_opcode == 27) &&
					(p->data.evt_gatt_characteristic_value.characteristic == ota_control_characteristic))
				{
					if ((p->data.evt_gatt_characteristic_value.value.data[0] == 0x03)
						&& (p->data.evt_gatt_characteristic_value.value.data[1] == 0x00))
					{
						Error_OTA(p->data.evt_gatt_characteristic_value.value.data[2], p->data.evt_gatt_characteristic_value.value.data[3]);
						fflush(stdout);
					}
				}
				ota_change_state(OTA_END);
				break;
			default:
				break;
			}
			break;

		case OTA_BEGIN:
			switch (BGLIB_MSG_ID(p->header))
			{
			case gecko_evt_le_connection_closed_id:
				printf("connection closed, retrying.\n");
				fflush(stdout);
				// ota_change_state( OTA_CONNECT ); 
				ota_change_state(OTA_END);
				break;
			case gecko_evt_gatt_procedure_completed_id:
				printf("OK\n");
				fflush(stdout);
				ota_change_state(OTA_READY);
				break;
			// Error from Server
			case gecko_evt_gatt_characteristic_value_id:

				if ((p->data.evt_gatt_characteristic_value.att_opcode == 27) &&
					(p->data.evt_gatt_characteristic_value.characteristic == ota_control_characteristic))
				{
					if ((p->data.evt_gatt_characteristic_value.value.data[0] == 0x03)
						&& (p->data.evt_gatt_characteristic_value.value.data[1] == 0x00))
					{
						Error_OTA(p->data.evt_gatt_characteristic_value.value.data[2], p->data.evt_gatt_characteristic_value.value.data[3]);
						fflush(stdout);
					}
				}
				ota_change_state(OTA_END);
				break;
			default:
				break;
			}
			break;

		case OTA_READY:
			switch (BGLIB_MSG_ID(p->header))
			{
			case gecko_evt_gatt_characteristic_value_id:
				if ((p->data.evt_gatt_characteristic_value.att_opcode == 27) &&
					(p->data.evt_gatt_characteristic_value.characteristic == ota_control_characteristic))
				{
					if (p->data.evt_gatt_characteristic_value.value.data[0] == 0x02)
					{
						printf("OK\n");
						fflush(stdout);
						ota_change_state(OTA_UPLOAD);
					}
					else if ((p->data.evt_gatt_characteristic_value.value.data[0] == 0x03)
						&& (p->data.evt_gatt_characteristic_value.value.data[1] == 0x00))
					{
						Error_OTA(p->data.evt_gatt_characteristic_value.value.data[2], p->data.evt_gatt_characteristic_value.value.data[3]);
						fflush(stdout);
					}
				}
				break;
			default:
				break;
			}
			break;

		case OTA_UPLOAD:
			switch (BGLIB_MSG_ID(p->header))
			{
			case gecko_evt_gatt_procedure_completed_id:
				if (p->data.evt_gatt_procedure_completed.result)
					ERROR_EXIT("procedure failed:%x\r\n", p->data.evt_gatt_procedure_completed.result);
				send_dfu_block();
				break;
			// Error from Server
			case gecko_evt_gatt_characteristic_value_id:

				if ((p->data.evt_gatt_characteristic_value.att_opcode == 27) &&
					(p->data.evt_gatt_characteristic_value.characteristic == ota_control_characteristic))
				{
					if ((p->data.evt_gatt_characteristic_value.value.data[0] == 0x03)
						&& (p->data.evt_gatt_characteristic_value.value.data[1] == 0x00))
					{
						Error_OTA(p->data.evt_gatt_characteristic_value.value.data[2], p->data.evt_gatt_characteristic_value.value.data[3]);
						fflush(stdout);
					}
				}
				ota_change_state(OTA_END);
				break;
			default:
				break;
			}
			break;

		case OTA_END:
			if(timeout)
			{
				printf("Server will reboot.");
				exit(EXIT_SUCCESS);
			}
			else
			{
				switch (BGLIB_MSG_ID(p->header))
				{
				case gecko_evt_le_connection_closed_id:
					// Close the connection
					printf("OK\n");
					printf("\n<----------------------- OTA EXIT ----------------------->\n");
					fflush(stdout);

					exit(EXIT_SUCCESS);

					break;
				case gecko_evt_gatt_procedure_completed_id:
					printf("OK\n");
					printf("\n<----------------------- OTA EXIT ----------------------->\n");
					fflush(stdout);

					exit(EXIT_SUCCESS);
					break;
					// Error from Server
				case gecko_evt_gatt_characteristic_value_id:
					if ((p->data.evt_gatt_characteristic_value.att_opcode == 27) &&
						(p->data.evt_gatt_characteristic_value.characteristic == ota_control_characteristic))
					{
						if ((p->data.evt_gatt_characteristic_value.value.data[0] == 0x03)
							&& (p->data.evt_gatt_characteristic_value.value.data[1] == 0x00))
						{
							Error_OTA(p->data.evt_gatt_characteristic_value.value.data[2], p->data.evt_gatt_characteristic_value.value.data[3]);
							

							// Close the connection
							printf("Closing connection...");
							fflush(stdout);
							{
								struct gecko_msg_endpoint_close_rsp_t  *rsp;
								rsp = gecko_cmd_endpoint_close(ble_connection);
								if (rsp->result)
									ERROR_EXIT("Error," __FUNCTION__ ", close failed,%x", rsp->result);
							}
						}
					}
					break;

				default:
					break;
				}
			}
			break;

		default:
			break;
		}
	}

	// File has been opened in hw_init()
	fclose(dfu_file);
	return(0);
}
